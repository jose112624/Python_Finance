Python for Finance
======

his project is a collection of jupyter notebook that I did from using the book Python for Finance: [Analyze Big Financial Data](https://smile.amazon.com/Python-Finance-Analyze-Financial-Data-ebook/dp/B00QUBHNBW/).

Command line instructions
======

Git global setup
------
```
git config --global user.name "Jose"
git config --global user.email "jegarciab@gmail.com"
```

Create a new repository
------
```
git clone https://gitlab.com/jose112624/Python_Finance.git
cd Python_Finance
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
Existing folder
------
```
cd existing_folder
git init
git remote add origin https://gitlab.com/jose112624/Python_Finance.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

Existing Git repository
------
```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/jose112624/Python_Finance.git
git push -u origin --all
git push -u origin --tags
```