Chapter 12. Excel Integration
======

Further Reading
------

For all libraries and solutions presented in this chapter, there are helpful web resources
available:
* For xlrd and xlwt, see http://www.python-excel.org for the online documentation; there is also a tutorial available in PDF format at http://www.simplistix.co.uk/presentations/python-excel.pdf.
* xlsxwriter is nicely documented on the website http://xlsxwriter.readthedocs.org.
* OpenPyxl has its home here: http://pythonhosted.org/openpyxl/.
* For detailed information about PyXLL, see https://www.pyxll.com.
* Free trials and detailed documentation for DataNitro can be found at http://www.datanitro.com.
* You can find the documentation and everything else you need regarding xlwings at http://xlwings.org.


Chapter 13. Object Orientation and Graphical User Interfaces
======
* Installed traits, trainsui

Further Reading
------

* The Python class documentation: https://docs.python.org/2/tutorial/classes.html
* The traits documentation: http://code.enthought.com/projects/traits/docs/html/index.html

Helpful resources in book form are:
* Downey, Allen (2012): Think Python. O’Reilly, Sebastopol, CA.
* Goodrich, Michael et al. (2013): Data Structures and Algorithms in Python. John Wiley & Sons, Hoboken, NJ.
* Langtangen, Hans Petter (2009): A Primer on Scientific Programming with Python. Springer Verlag, Berlin, Heidelberg.


Chapter 14. Web Integration
======
* protocolos: urllib, httplib, ftplib, 
* charting : bokeh
* web: djando, flask, pyramis/pylons, turbogerars, zope
* web services: 

Example: reading from a [feed](http://hopey.netfonds.no/posdump.php?date=20171215&paper=AAPL.O&csv_format=csv)

Further Reading
------
Further Reading

The following web resources are helpful with regard to the topics covered in this chapter:
* The Python documentation should be a starting point for the basic tools and techniques shown in this chapter: http://docs.python.org; see also this overview page:http://docs.python.org/2/howto/webservers.html.
* You should consult the home page of Bokeh for more on this webfocused plotting library: http://bokeh.pydata.org.
* For more on Flask, start with the home page of the framework: http://flask.pocoo.org; also, download the PDF documentation: https://media.readthedocs.org/pdf/flask/latest/flask.pdf.
* Apart from the Python documentation itself, consult the home page of the Werkzeug library for more on web services: http://werkzeug.pocoo.org.

For a Flask reference in book form, see the following:
* Grinberg, Miguel (2014): Flask Web Development — Developing Web Applications with Python. O’Reilly, Sebastopol, CA.

Finally, here is the research paper about the valuation of volatility options:

* Gruenbichler, Andreas and Francis Longstaff (1996): “Valuing Futures and Options on Volatility.” Journal of Banking and Finance, Vol. 20, pp. 985–1001.