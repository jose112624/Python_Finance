Setup
====

This is the setup to run these notebooks

Anaconda
------
* Install Python 3.6 version : https://www.anaconda.com/download/


conda forge
------

https://conda-forge.org/#about

```
conda config --add channels conda-forge 
conda install <package-name>
```


jupyter Notebook Extensions
------
* [docs](http://jupyter-contrib-nbextensions.readthedocs.io/en/latest )
* [GitHub](https://github.com/ipython-contrib/jupyter_contrib_nbextensions) repo
* install :
	1. `conda install -c conda-forge jupyter_contrib_nbextensions`
	2. `jupyter contrib nbextension install --user`
	3. [configure](http://jupyter-contrib-nbextensions.readthedocs.io/en/latest/install.html#enabling-disabling-extensions) extensions on notebook url: e.g. http://localhost:8888/nbextensions, they will also appear as another tab in the jupyter notebook home page


		

